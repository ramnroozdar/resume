<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('mobile')->unique();
            $table->string('email')->unique();
            $table->enum('role',['admin','user','staff', 'marketer'])->default('user');
            $table->string('password');

            //Other Information
            $table->text('status')->nullable();
            $table->enum('gender',['male','female'])->nullable();
            $table->enum('soldier',[1 , 2 , 3 , 4 , 5])->default('1');
            $table->string('birth')->nullable();
            $table->enum('material',['married','single'])->default('single');
            $table->string('national_code')->unique()->nullable();
            $table->string('birth_certificate_code')->nullable();
            $table->string('phone')->nullable();
            $table->string('zip_code')->nullable();
            $table->integer('province_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->text('address')->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
        DB::table('users') -> insert([

            'name' => 'رامین روزدار',
            'mobile' => '09337288808',
            'email' => 'raminroozdar@gmail.com',
            'role' => 'admin',
            'password' => Hash::make('09337288808'),
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
