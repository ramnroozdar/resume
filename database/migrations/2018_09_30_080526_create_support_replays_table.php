<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportReplaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_replays', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('support_id');
            $table->text('text');
            $table->enum('type',['normal','system','forward'])->default('normal');
            $table->enum('status',['new','reject','accept'])->default('new');
            $table->ipAddress('ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_replays');
    }
}
