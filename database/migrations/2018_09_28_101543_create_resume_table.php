<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resume', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('category_id');
            $table->string('resume');
            $table->string('name')->nullable();
            $table->string('file_type')->nullable();
            $table->double('size')->nullable();
            $table->enum('Last_grade',[1 , 2 , 3 , 4 , 5 , 6])->default(6);
            $table->enum('introduction',[1 , 2 , 3 , 4 , 5 , 6])->default(6);
            $table->string('field');
            $table->string('orientation');
            $table->string('university');
            $table->string('skill')->nullable();
            $table->string('certificate')->nullable();
            $table->string('samples')->nullable();
            $table->string('description')->nullable();
            $table->string('rights')->nullable();
            $table->enum('status',[1 , 2 , 3 , 4 , 5 , 6 , 7])->default('1');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resume');
    }
}
