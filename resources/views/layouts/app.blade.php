<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title'){{ config('platform.name') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="rtl">
    <div id="app">
        <header class="{{ config('platform.header-position') }}">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand mr-auto" href="{{ url('/') }}">
                        <i class="fa {{ config('platform.main-icon') }} ml-2"></i> {{ config('platform.name') }}
                    </a>
                    @guest
                        <div class="d-block d-sm-none"><button class="btn btn-outline-secondary" data-toggle="modal" data-target="#loginModal"><i class="fa fa-sign-in ml-1"></i>ورود </button></div>
                    @endguest
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                            @guest

                            @endguest
                            @auth
                                @if(Auth::user()->role == 'admin')
                                    <li><a class="nav-link{{ Request::segment(2) == 'dashboard' ? ' active' : '' }}" href="{{ route('admin.dashboard') }}"> <i class="fa fa-cogs"></i>مدیریت سیستم </a></li>
                                @endif
                                    <li><a class="nav-link{{ Request::segment(3) == 'dashboard' ? ' active' : '' }}" href="{{ route('dashboard') }}"> <i class="fa fa-dashboard"></i> پنل کابری </a></li>
                                @endauth
                        </ul>
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <button class=" btn btn-info"  data-toggle="modal" data-target="#loginModal" href=""><li class="fa fa-sign-in ml-1"></li> ورود </button>
                                {{--<button class=" btn btn-primary  ml-2" data-toggle="modal" data-target="#registerModal"><i class="fa fa-user-plus ml-1"></i> ثبت نام </button>--}}
                            @else
                                <li><a class="nav-link{{ Request::segment(2) == 'notification' ? ' active' : '' }}" href="{{ route('frontend.notification') }}"><i class="fa fa-bell"></i>اطلاعیه ها <notification-navbar-component></notification-navbar-component></a></li>
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                        <i class="fa fa-user-circle-o"></i> {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('frontend.users.password') }}"> <i class="fa fa-key"></i> تغییر رمز عبور  </a>
                                        <a class="dropdown-item" href="{{ route('users.profile') }}"><i class="fa fa-user-circle"></i> پروفایل من</a>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out"></i>
                                            خروج
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <main class="py-4" role="main">
            <div class="{{ config('platform.main-container') }}">
                @include('partials.errors')
                @include('partials.notifications')

                @yield('content')
            </div>
        </main>
    </div>
    @guest
        @include('partials.modal-login')
        @include('partials.modal-register')
    @endguest

    @yield('js')
</body>
<footer dir="rtl" class="page-footer font-small pt-4 mt-4 text-white navbar-expand-md navbar-dark bg-dark">
    <div class="{{ config('platform.navbar-container') }}">
        @include('partials.footer')
    </div>
</footer>
</html>
