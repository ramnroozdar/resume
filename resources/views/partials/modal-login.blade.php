<div class="modal" tabindex="-1" role="dialog" id="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-white" style="background-color: #0F518E;">
                <h5 class="modal-title">
                    <i class="fa fa-sign-in"></i>
                    ورود
                </h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="login" class="col-sm-4 col-form-label text-md-right">آدرس ایمیل یا موبایل</label>

                        <div class="col-md-7">
                            <input id="login" type="login" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }} text-md-left" name="login" value="{{ old('login') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">رمز عبور</label>

                        <div class="col-md-7">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6 offset-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    به یاد آوردن
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-9 offset-md-4">
                            <button type="submit" class="col-md-9 btn btn-block">
                                ورود
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success  col-md-6" href="{{ route('password.request') }}"><i
                            class="fa fa-refresh ml-1"></i>بازیابی رمز عبور</a>
                <a class="btn btn-primary  col-md-6" href="{{ route('register') }}"><i
                            class="fa fa-user-plus ml-1"></i>ثبت نام</a>
            </div>
        </div>
    </div>
</div>