<div class="nav-scroller bg-white box-shadow">
    <nav class="nav nav-underline">
        <a class="nav-link{{ (Request::segment(2) == 'user') ? ' active' : '' }}" href="{{ route('admin.users.list') }}"><i class="fa fa-users"></i> کاربرها</a>
        <a class="nav-link {{ (Request::segment(2) == 'file') ? ' active' : '' }}" href="{{ route('admin.files.list') }}">
            <i class="fa fa-files-o"></i>  فایل ها
            <span class="badge badge-pill bg-light align-text-bottom">0</span>
        </a>
        <a class="nav-link {{ (Request::segment(2) == 'category' ) ? ' active' : '' }}" href="{{ route('admin.categories.list') }}"><i class="fa fa-object-group"></i> دسته ها</a>
        <a class="nav-link {{ (Request::segment(2) == 'transaction' ) ? ' active' : '' }}" href="{{route('admin.payments.list')}}"><i class="fa fa-money"></i> تراکنش ها </a>
        <a class="nav-link {{ (Request::segment(2) == 'ticket' ) ? ' active' : '' }}" href="{{ route('admin.tickets') }}"><i class="fa fa-life-ring"></i> پشتیبانی</a>
        <a class="nav-link {{ (Request::segment(2) == 'report' ) ? ' active' : '' }}" href="{{ route('admin.reports.index') }}"><i class="fa fa-send-o"></i> گزارشات</a>
        <a class="nav-link {{ (Request::segment(2) == 'article' ) ? ' active' : '' }}" href="{{ route('admin.articles') }}"><i class="fa fa-newspaper-o"></i> مقاله ها</a>
        <a class="nav-link" href="#"><i class="fa fa-bars"></i> فاکتورها</a>
        <a class="nav-link" href="#"><i class="fa fa-window-restore"></i> صفحه ها</a>
        <a class="nav-link" href="#"><i class="fa fa-cog fa-spin"></i>  تنظیمات</a>
    </nav>
</div>

