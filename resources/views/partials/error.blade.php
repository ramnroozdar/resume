@if(session('error'))
    <div class="alert alert-danger">
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ session('error') }}</p>
    </div>
@endif