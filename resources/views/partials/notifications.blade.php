@if(session('success'))
    <div class="alert alert-success">
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ session('success') }}</p>
    </div>
    @endif