<div class="container text-center text-md-right" style="height: 200px">
<div class="text-center">
    <ul class="list-unstyled list-inline">
        <li class="list-inline-item">
            <a class="btn-floating btn-sm btn-fb mx-1">
                <i class="fa fa-facebook"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-sm btn-tw mx-1">
                <i class="fa fa-twitter"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-sm btn-gplus mx-1">
                <i class="fa fa-google-plus"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-sm btn-li mx-1">
                <i class="fa fa-linkedin"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-sm btn-dribbble mx-1">
                <i class="fa fa-dribbble"> </i>
            </a>
        </li>
    </ul>
</div>
<div class="footer-copyright py-3 text-center">
    تمام حقوق این سایت متعلق به شرکت
    <a href="https://sabanovin.com">صبانوین جام جم </a>
    می باشد.
</div>
</div>