<div class="modal" tabindex="-1" role="dialog" id="CategoryModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-white" style="background-color: #0F518E;">
                <h5 class="modal-title">
                    <i class="fa fa-align-justify"></i>
                    دسته بندی ها
                </h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body">

                            <div class="row justify-content-center mb-2">
                                <ul class="navbar-nav mr-auto">
                                        @foreach($categories as $category)
                                            @if($category->category_id == 0)
                                                <li><a href="{{route('frontend.category.single',[$category->id])}}">{{$category->category_name }}</a></li>
                                            @endif
                                            @foreach($category->children as $child )
                                                    <li><a href="{{route('frontend.category.single',[$child->id])}}">&nbsp; &nbsp;&nbsp;{{$child->category_name}}</a></li>
                                                @endforeach
                                        @endforeach
                                </ul>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>