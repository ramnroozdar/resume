<div class="list-group mb-2 d-none d-md-block d-lg-block d-xl-block">
    <a class="list-group-item list-group-item-action{{ Request::segment(1) == 'dashboard' ? ' active' : '' }}" href="{{ route('frontend.users.dashboard') }}"><i class="fa fa-dashboard"></i> داشبرد</a>
    <a class="list-group-item list-group-item-action{{ Request::segment(1) == 'files' ? ' active' : '' }}" href="{{ route('frontend.files.manage') }}"><i class="fa fa-files-o"></i> فایل ها</a>
    <a class="list-group-item list-group-item-action{{ Request::segment(1) == 'ticket' ? ' active' : '' }}" href="{{route('frontend.tickets')}}"><i class="fa fa-life-ring"></i> پشتیبانی</a>
    <a class="list-group-item list-group-item-action{{ Request::segment(1) == 'invoice' ? ' active' : '' }}" href="#"><i class="fa fa-bars"></i> فاکتورها</a>
    <a class="list-group-item list-group-item-action{{ Request::segment(1) == 'transaction' ? ' active' : '' }}" href="{{ route('frontend.transactions') }}"><i class="fa fa-money"></i> تراکنش ها</a>
   {{-- @foreach($menus as $menu)
        @if($menu->type == 'user')
            <a class="list-group-item list-group-item-action{{ Request::segment(1) == $menu->route ? ' active' : '' }}" href="{{ route($menu->route) }}"><i class="{{ $menu->icon }}"></i> {{ $menu->title }}</a>
        @endif
    @endforeach--}}
</div>
