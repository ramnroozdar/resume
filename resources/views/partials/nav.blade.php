<header class="{{ config('platform.header-position') }}">
<nav class="navbar navbar-expand-md navbar-dark" style="background-color: #0F518E;">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand mr-auto" href="{{ url('/') }}">
            <i class="fa {{ config('platform.main-icon') }} ml-2"></i> {{ config('platform.name') }}
        </a>
        @guest
            <div class="d-block d-sm-none"><button class="btn btn-outline-secondary" data-toggle="modal" data-target="#loginModal"><i class="fa fa-sign-in ml-1"></i>ورود / ثبت نام </button></div>
        @endguest
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

                    @auth
                        @if(Auth::user()->role == 'admin')
                            <li><a class="nav-link{{ Request::segment(2) == 'dashboard' ? ' active' : '' }}" href="{{ route('admin.dashboard') }}"> <i class="fa fa-cogs"></i>مدیریت سیستم </a></li>
                        @endif
                    @endauth
                    @auth
                        <li><a class="nav-link{{ Request::segment(3) == 'dashboard' ? ' active' : '' }}" href="{{ route('frontend.dashboard') }}"> <i class="fa fa-dashboard"></i> پنل کابری </a></li>
                    @endauth
                    <a class="nav-link d-block text-white" href="#" data-toggle="modal" data-target="#CategoryModal"><i class="fa fa-align-justify mr-1"></i>دسته ها </a>
                    <a class="nav-link d-block text-white" href="{{ route('frontend.files') }}"><i class="fa fa-files-o mr-1"></i>دوره ها </a>
                    <a class="nav-link d-block text-white" href="{{ route('frontend.sessions.list') }}"><i class="fa fa-file-movie-o mr-1"></i> جلسات </a>
                    <a class="nav-link d-block text-white" href="{{ route('frontend.article') }}"><i class="fa fa-newspaper-o mr-1"></i>مقاله ها </a>
            </ul>
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <button class=" btn btn-success"  data-toggle="modal" data-target="#loginModal" href=""><li class="fa fa-sign-in ml-1"></li> ورود </button>
                    <a class=" btn btn-primary  ml-2" href="{{ route('register') }}"> <i class="fa fa-user-plus ml-1"></i> ثبت نام </a>
                @else
                    <li><a class="nav-link{{ Request::segment(2) == 'notification' ? ' active' : '' }}" href="{{ route('frontend.notification') }}"><i class="fa fa-bell"></i>اطلاعیه ها <notification-navbar-component></notification-navbar-component></a></li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <i class="fa fa-user-circle-o"></i> {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('frontend.users.password') }}"> <i class="fa fa-key"></i> تغییر رمز عبور  </a>
                            <a class="dropdown-item" href="{{ route('frontend.users.profile') }}"><i class="fa fa-user-circle"></i> پروفایل من</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i>
                                خروج
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
</header>

