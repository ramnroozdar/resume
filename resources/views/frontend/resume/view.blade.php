@extends('layouts.app')

@section('title', " رزومه شماره ", $resume->id)

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ config('platform.name') }}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">پنل کاربری</a></li>
                    <li class="breadcrumb-item" ><a href="{{ route('frontend.resumes') }}">رزومه ها</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('frontend.resumes.view',['id',$resume->id]) }}">جزئیات رزومه</a></li>
                </ol>
            </nav>
        </div>
        <div class="col-md-12">
            <h1>رزومه شماره:{{$resume->id}}</h1>
        </div>
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    جزئیات رزومه
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">شماره رزومه:</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->id}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">شغل:</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->category->name}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">نام و نام خانوادگی:</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->user->name}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">نحوه آشنایی با ما :</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{ constant('App\Enums\FileEnum::INTRODUCTION_'.strtoupper($resume -> introduction).'_TEXT')}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">وضعیت :</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{ constant('App\Enums\FileEnum::STATUS_'.($resume -> status).'_TEXT')}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">نام فایل :</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->name}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">نوع فایل :</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->file_type}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">سایز فایل :</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->size}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">تاریخ ارسال :</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{ jDate($resume->created_at)->format('Y/m/d') }}
                        </div>
                    </div>

                </div>
            </div>
            <div class="card">
                <div class="card-header badge-danger h5">
                    پشتیبانی
                </div>
                @foreach($supports as $support)
                    @if ($support->resume_id == $resume->id)
                        @if (Auth::user()->id == $support->user_id )
                            <div class="card card-default bg-secondary mb-2 mt-2">
                                <div class="card-header">
             <span class="badge badge-dark pull-left">
                 {{ $support->created_at }}
             </span>
                                    <div class="row">
                <span class="mt-2">
                    {{ $support->user->name }}
                </span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    {!! nl2br($support->comment)  !!}
                                    <form id="comment_{{$support->id}}" style="display: none">
                                        <textarea class="form-control"></textarea>
                                        <button>ثبت</button>
                                    </form>
                                </div>
                            </div>
                        @else
                            <div class="card card-default ml-5 mb-2 bg-success">
                                <div class="card-header">
                                    <span class="badge badge-dark pull-left">{{ $support->created_at }}</span>
                                    <div class="row">
                <span class="mt-2 text-white">
                      <i class="fa fa-headphones fa-2x text-secondary"></i>{{ $support->user->name }}  (پشتیبانی)
                </span>
                                    </div>
                                </div>
                                <div class="card-body text-white">
                                    {!! nl2br($support->comment)  !!}
                                </div>
                                {{ $supports->links() }}
                            </div>
                        @endif
                    @endif
                @endforeach
                <div class="card card-default">
                    <div class="card-header">
                        @if (Auth::user()->role == 'admin')
                            پاسخ به کاربر
                        @else
                            ارسال پیام به پشتیبان در مورد این شغل
                        @endif
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('frontend.resumes.comment',['id'=>$resume->id]) }}" enctype="multipart/form-data">
                            @csrf
                            @method('post')
                            <div class="form-group">
                                <label for="text">متن</label>
                                <textarea name="comment" id="comment" class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}">{{old('comment')}}</textarea>
                                @if ($errors->has('comment'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('comment') }}</strong></span>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary btn-mobile"><i class="fa fa-send"></i>ارسال پیام</button>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>

@stop


