@extends('layouts.app')

@section('title','رزومه ')

@section('content')
    <div class="col-md-12">

        <div class="jumbotron">
            <h4 class="display-6">
                    سلام کاربر گرامی
            </h4>
            <p class="lead"> رزومه شما ارسال گردید... برای اطلاع از وضعیت رزومه ، تکمیل مشخصات و ارسال رزومه جدید می توانید وارد سیستم شوید</p>
            <hr class="my-4">
            <p class="alert alert-danger">نکته : نام کاربری و رمز عبور شما : شماره همراهی می باشد که در هنگام ارسال رزومه وارد کرده اید...</p>
            <ul>

            </ul>
            <div class="col-md-12 row justify-content-center">

                    <a class="btn btn-warning btn-lg btn-sm btn-block mr-1" href="{{ route('login') }}" role="button"><i class="fa fa-sign-in"></i> ورود به سیستم  </a>

            </div>

        </div>

    </div>
    @endsection