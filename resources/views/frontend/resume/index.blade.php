@extends('layouts.app')

@section('title', "لیست رزومه ها")

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ config('platform.name') }}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">پنل کاربری</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('admin.resumes') }}">رزومه ها</a></li>
                </ol>
            </nav>
        </div>
        <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">
                        <div class="col-md-4 pull-left">
                            <a href="{{ route('users.resume') }}" class="btn btn-danger btn-sm btn-block  ml-1"><i
                                        class="fa fa-file"></i>  ارسال رزومه  </a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered table-hover">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col" class="text-center">شغل مورد نظر</th>
                                <th scope="col" class="text-center">وضعیت</th>
                                <th scope="col" class="text-center">اقدام ها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($resumes && count($resumes)>0)
                            @foreach($resumes as $resume)
                                <tr>
                                    <td scope="row" class="text-center">{{ $resume->category->name }}</td>
                                    <td class="text-center">{{ constant('App\Enums\FileEnum::STATUS_'.($resume -> status).'_TEXT')}}</td>
                                    <td>
                                        {{--<button type="button" onclick="$('#viewResume').toggle();" class="btn btn-mobile btn-success btn-sm"><i class="fa fa-eye"></i> نمایش</button>--}}
                                        <a href="{{ route('frontend.resumes.view',['id'=> $resume->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i> نمایش</a>
                                        {{--<a href="{{ route('admin.resumes.download',['id'=> $resume->id]) }}" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> دانلود</a>--}}
                                    </td>
                                </tr>

                            @endforeach
                            @else
                                <tr>
                                    <td colspan="3">
                                        <span>هیچ رزومه ای وجود ندارد </span>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        {{ $resumes->links() }}
                    </div>
                </div>
            

        </div>
    </div>
    </div>
    </div>

@stop