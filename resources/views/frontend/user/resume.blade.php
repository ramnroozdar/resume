@extends('layouts.app')

@section('content')

    <div class="container_fulid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('partials.error')
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{route('resume.insert')}}" enctype="multipart/form-data">
                            @csrf
                            @method('post')
                            <div class="alert alert-success mt-2 text-md-center">
                                <span>فرم درخواست همکاری با صبانوین</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="job">
                                        شغل مورد نظر :</label>
                                    <select name="job" id="job" class="form-control{{ $errors->has('job') ? ' is-invalid' : '' }}">
                                        @foreach($categories as $category)
                                            <option value="{{  $category->id }}">{{ $category->name  }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('job'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('job') }}</strong></span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="introduction">
                                        نحوه آشنایی :</label>
                                    <select  name="introduction" id="introduction" class="form-control{{ $errors->has('introduction') ? ' is-invalid' : '' }}"   >
                                        <option value="6"{{ old('introduction') == '6' ? ' selected' :'' }}>سایر</option>
                                        <option value="1"{{ old('introduction') == '1' ? ' selected' :'' }}>موتورهای جستجو</option>
                                        <option value="2"{{ old('introduction') == '2' ? ' selected' :'' }}>معرفی دوستان</option>
                                        <option value="3"{{ old('introduction') == '3' ? ' selected' :'' }}>تبلیغات در سایت ها</option>
                                        <option value="4"{{ old('introduction') == '4' ? ' selected' :'' }}>ایمیل تبلیغاتی</option>
                                        <option value="5"{{ old('introduction') == '5' ? ' selected' :'' }}>پیامک تبلیغاتی</option>
                                    </select>
                                    @if ($errors->has('introduction'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('introduction') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-sm-6 form-group">
                                <label for="resume">
                                    آپلود فایل رزومه :</label>
                                <input type="file" class="form-control" id="resume" name="resume"
                                       maxlength="50">
                            </div>
                            <div class="col-sm-6 form-group mt-4">
                                <div class="alert alert-primary p-2" role="alert">
                                    ارسال فایل رزومه اجباری می باشد.
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <button type="submit" class="btn btn-lg btn-success btn-block" id="btnContactUs"> <i class="fa fa-id-card mr-2"></i>ارسال رزومه </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')

@endsection


