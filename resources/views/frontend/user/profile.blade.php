@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="alert alert-success mt-2 text-md-center">
                        <span>مشخصات کاربری</span>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{route('frontend.users.updateProfile')}}">
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="name">
                                        نام و نام خانوادگی :</label>
                                    <input type="text" class="form-control" id="name" name="name"  maxlength="50" value="{{ old('name',Auth::user()->name) }}"readonly>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="email">
                                        آدرس ایمیل :</label>
                                    <input type="text" class="form-control" id="email" name="email"  maxlength="50" value="{{ old('email',Auth::user()->email) }}"readonly>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="mobile">
                                        شماره همراه :</label>
                                    <input type="text" class="form-control" id="mobile" name="mobile"  maxlength="50" value="{{ old('mobile',Auth::user()->mobile) }}"readonly>
                                    @if ($errors->has('mobile'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group mt-2">
                                    <label for="mobile"></label>
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <i class="fa fa-save"></i>
                                        بروز رسانی پروفایل
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="alert alert-warning mt-2 text-md-center">
                        <span>مشخصات تکمیلی </span>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{route('frontend.users.updateInformation')}}" enctype="multipart/form-data">
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="gender">
                                        جنسیت :</label>
                                    <select class="select2 form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" required>
                                        <option value="male"{{ old('gender', Auth::user()->gender) == 'male' ? ' selected' :'' }}>مرد</option>
                                        <option value="female"{{ old('gender', Auth::user()->gender) == 'female' ? ' selected' :'' }}>زن</option>
                                    </select>

                                    @if ($errors->has('gender'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="soldier	">
                                        وضعیت نظام وظیفه :</label>
                                    <select class="select2 form-control{{ $errors->has('soldier	') ? ' is-invalid' : '' }}" name="soldier" required>
                                        <option value="1"{{ old('soldier', Auth::user()->soldier) == '1' ? ' selected' :'' }}>پایان خدمت</option>
                                        <option value="2"{{ old('soldier', Auth::user()->soldier) == '2' ? ' selected' :'' }}>معافیت پزشکی</option>
                                        <option value="3"{{ old('soldier', Auth::user()->soldier) == '3' ? ' selected' :'' }}>سایر معافیت ها</option>
                                        <option value="4"{{ old('soldier', Auth::user()->soldier) == '4' ? ' selected' :'' }}>انجام نشده</option>
                                        <option value="5"{{ old('soldier', Auth::user()->soldier) == '5' ? ' selected' :'' }}>زن می باشم</option>
                                    </select>

                                    @if ($errors->has('soldier'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('soldier') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="material">
                                        وضعیت تاهل :</label>
                                    <select class="form-control{{ $errors->has('material') ? ' is-invalid' : '' }}" name="material" required>
                                        <option value="single"{{ old('material', Auth::user()->material) == 'single' ? ' selected' :'' }}>مجرد</option>
                                        <option value="married"{{ old('material', Auth::user()->material) == 'married' ? ' selected' :'' }}>متاهل</option>
                                    </select>

                                    @if ($errors->has('material'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('material') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="birth">
                                        تاریخ تولد :</label>
                                    <input type="text" class="form-control" id="birth" name="birth"  maxlength="50" value="{{ old('birth',Auth::user()->birth) }}"placeholder="11/06/1375">
                                    @if ($errors->has('birth'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('birth') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="national_code">
                                        کد ملی :</label>
                                    <input type="text" class="form-control" id="national_code" name="national_code"  maxlength="50" value="{{ old('national_code',Auth::user()->national_code) }}">
                                    @if ($errors->has('national_code'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('national_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="birth_certificate_code">
                                        شماره شناسنامه :</label>
                                    <input type="text" class="form-control" id="birth_certificate_code" name="birth_certificate_code"  maxlength="50" value="{{ old('birth_certificate_code',Auth::user()->birth_certificate_code) }}">
                                    @if ($errors->has('birth_certificate_code'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('birth_certificate_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="phone">
                                        تلفن ثابت :</label>
                                    <input type="text" class="form-control" id="phone" name="phone"  maxlength="50" value="{{ old('phone',Auth::user()->phone) }}">
                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="zip_code">
                                        کد پستی :</label>
                                    <input type="text" class="form-control" id="zip_code" name="zip_code"  maxlength="50" value="{{ old('zip_code',Auth::user()->zip_code) }}">
                                    @if ($errors->has('zip_code'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="province_id">
                                        استان :</label>
                                    <select onchange="selectProvince(this.value);" class="select2 form-control{{ $errors->has('province_id') ? ' is-invalid' : '' }}" name="province_id" required>
                                        @foreach($provinces as $province)
                                            <option value="{{ $province->id }}"{{ old('province_id', Auth::user()->province_id) == $province->id ? ' selected' :'' }}>{{ $province->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('province_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('province_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="city_id">
                                        شهر :</label>
                                    <select id="city_id" class="select2 form-control{{ $errors->has('city_id') ? ' is-invalid' : '' }}" name="city_id" required>
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}"{{ old('city_id', Auth::user()->city_id) == $city->id ? ' selected' :'' }}>{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('city_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="address">
                                        آدرس :</label>
                                    <textarea class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="" required>{{ old('address', Auth::user()->address) }}</textarea>
                                @if ($errors->has('address'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="status">
                                        بیوگرافی :</label>
                                    <textarea placeholder="بیو گرافی کوتاهی از خود بنویسید" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" value="" required>{{ old('status', Auth::user()->status) }}</textarea>
                                @if ($errors->has('status'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-6 form-group mt-2">
                                    <label for="status"> </label>
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <i class="fa fa-save"></i>
                                        تکمیل مشخصات
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        function selectProvince(province_id) {
            axios.post('{{ route('frontend.users.profile.cities') }}',{'province_id':province_id}).then(function (response) {
                $('#city_id').html('');
                for (var i = 0, len = response.data.length; i < len; i++) {
                    var city = new Option(response.data[i].name, response.data[i].id, false, false);
                    $('#city_id').append(city).trigger('change');
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    </script>
@endsection




