@extends('layouts.app')

@section('content')
    <main class="py-4" role="main">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ config('platform.name') }}</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="{{route('frontend.users.dashboard')}}">داشبرد</a></li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-12">
                    <h1>داشبرد</h1>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="list-group mb-2d-none d-md-block d-lg-block d-xl-block">
                        <a class="list-group-item list-group-item-action active " href="#"><i class="fa fa-dashboard"></i> داشبرد</a>
                        <a class="list-group-item list-group-item-action" href="{{ route('users.profile') }}"><i class="fa fa-user-circle"></i> پروفایل </a>
                        <a class="list-group-item list-group-item-action" href="{{ route('frontend.resumes') }}"><i class="fa fa-files-o"></i> رزومه ها</a>
                        <a class="list-group-item list-group-item-action" href="{{ route('frontend.users.password') }}"><i class="fa fa-bars"></i> تغییر رمز عبور</a>
                    </div>
                </div>
                <div class="col-md-9">

                    <div class="row justify-content-center">
                        <div class="col-md-12 col-xs-12">
                            <div class="alert alert-info">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                امروز {{ jDate(now())->format('Y/m/d') }}
                            </div>
                        </div>

                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <a class="mb-1 btn btn-info btn-block" href="{{ route('users.resume') }}" ><i class="fa fa-file"></i> ارسال رزومه جدید </a>
                        </div>
                        <div class="col-md-4">
                            <a class="mb-1 btn btn-success btn-block" href="{{ route('frontend.resumes') }}" ><i class="fa fa-files-o"></i> رزومه های من </a>
                        </div>
                        <div class="col-md-4">
                            <a class="mb-1 btn btn-danger btn-block" href="#"><i class="fa fa-plus"></i> ارسال تیکت</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </main>

@endsection
