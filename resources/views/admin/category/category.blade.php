<li class="list-unstyled">
    <span class="btn btn-sm btn-dark">{{ $category->name}}</span>
    <a class="btn btn-sm btn-success" href="{{ route('admin.categories.edit',[$category->id])}}"><i class="fa fa-edit"></i> ویرایش دسته </a>
    @if($category->status == 'accepted')
        <form action="{{ route('admin.categories.disable',[$category->id]) }}" method="post" style="display: inline">
            @csrf
            @method('post')
            <button class="btn btn-danger btn-sm"><i class="fa fa-close"></i> غیرفعال کردن </button>
        </form>
    @endif
    @if($category->status == 'disable')
        <form action="{{ route('admin.categories.accepted',[$category->id]) }}" method="post" style="display: inline">
            @csrf
            @method('post')
            <button class="btn btn-primary btn-sm"><i class="fa fa-check-circle"></i> فعال کردن </button>
        </form>
    @endif
</li>
<div class="mb-1"></div>