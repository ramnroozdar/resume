@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ config('platform.name') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"> مدیریت سیستم </a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.categories')}}">  شغل ها </a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="#"> ایجاد شغل </a></li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header bg-secondary text-white">
                        ایجاد شغل
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.categories.insert')}}" method="post">
                            @Csrf
                            <div class="form-group">
                                <label for="name">عنوان شغل  :</label>
                                <input class="form-control" name="name" id="name">
                            </div>
                            <div class="form-group">
                                <label for="category_id">ترتیب :</label>
                                <input class="form-control" name="order" id="order">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-block" type="submit">ایجاد شغل </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection