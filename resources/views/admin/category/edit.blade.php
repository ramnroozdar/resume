@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ config('platform.name') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"> مدیریت سیستم </a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.categories')}}">  دسته ها </a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="#">ویرایش شغل :   {{ $category->name }}  </a></li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header bg-secondary text-white">
                        ویرایش شغل :   {{ $category->name }}
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.categories.update',[$category->id])}}" method="post">
                            @Csrf
                            <div class="form-group">
                                <label for="name">نام شغل  :</label>
                                <input class="form-control" name="name" id="name" value="{{ old('name', $category->name) }}">
                            </div>
                            <div class="form-group">
                                <label for="order">ترتیب :</label>
                                <input class="form-control" name="order" id="order" value="{{ old('order', $category->order) }}">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-block" type="submit">ذخیره تغییرات </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection