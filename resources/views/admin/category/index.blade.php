@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ config('platform.name') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"> مدیریت سیستم </a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="#"> شغل ها </a></li>
                    </ol>
                </nav>
                <div class="card card-default">
                    <div class="card-header"> شغل ها </div>
                    <div class="card-body">
                                <div class="card card-default">
                                    <div class="card-body">
                                        @if (count($categories) > 0)
                                            <ul>
                                                @foreach ($categories as $category)
                                                    @include('admin.category.category', $category)
                                                @endforeach
                                            </ul>
                                        @else
                                            دسته بندی برای نمایش وجود ندارد
                                        @endif
                                    </div>
                                    <div class="card-footer">
                                        <a href="{{route('admin.categories.create')}}" class="btn btn-dark btn-sm pull-right"><i
                                                    class="fa fa-plus-circle"></i> ایجاد شغل جدید </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection