@extends('layouts.app')

@section('title', "لیست رزومه ها")

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ config('platform.name') }}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">مدیریت سیستم</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="#">کاربران</a></li>
                </ol>
            </nav>
        </div>
        <div class="col-md-12">
            @if (isset($users))
                <div class="card card-default">
                    <div class="card-body">
                        <table class="table table-striped table-bordered table-hover">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col" class="text-center">#</th>
                                <th scope="col" class="text-center">نام و نام خانوادگی</th>
                                <th scope="col" class="text-center">تاریخ</th>
                                <th scope="col" class="text-center">اقدام ها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td scope="row" class="text-center">{{ $user->id }}</td>

                                    <td class="text-center table-danger">{{ $user->name}}</td>

                                    <td class="text-center">{{ $user->created_at }}</td>
                                    <td>
                                        <a href="{{ route('admin.users.view',['id'=> $user->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i> نمایش</a>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            @endif


        </div>
    </div>
    </div>
    </div>

@stop