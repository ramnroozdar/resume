@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">خانه</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"> مدیریت سیستم </a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.users') }}"> کابران </a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="#"> نمایش کاربر  {{ $user->name }}</a></li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header">  نمایش اطلاعات  {{ $user->name }} </div>

                    <div class="card-body">
                        <table id="users" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <tr>
                                <td>شناسه </td>
                                <td>{{ $user->id  }}</td>
                            </tr>
                            <tr>
                                <td>نام کامل</td>
                                <td>{{ $user->name  }}</td>
                            </tr>
                            <tr>
                                <td>موبایل</td>
                                <td>{{ $user->mobile  }}</td>
                            </tr>
                            <tr>
                                <td>ایمیل</td>
                                <td>{{ $user->email  }}</td>
                            </tr>
                            <tr>
                                <td>سطح کاربری</td>
                                <td>{{ constant('App\Enums\FileEnum::ROLE_'.strtoupper($user -> role).'_TEXT')}}</td>
                            </tr>
                            <tr>
                                <td>جنسیت</td>
                                <td>{{ constant('App\Enums\FileEnum::GENDER_'.strtoupper($user -> gender).'_TEXT')}}</td>
                            </tr>
                            <tr>
                                <td>وضعیت نظام وظیفه</td>
                                <td>{{ constant('App\Enums\FileEnum::SOLDIER_'.strtoupper($user -> soldier).'_TEXT')}}</td>
                            </tr>
                            <tr>
                                <td>وضعیت تاهل</td>
                                <td>{{ constant('App\Enums\FileEnum::MATERIAL_'.strtoupper($user -> material).'_TEXT')}}</td>
                            </tr>
                            <tr>
                                <td>تاریخ تولد</td>
                                <td>{{ $user->birth  }}</td>
                            </tr>
                            <tr>
                                <td>کد ملی</td>
                                <td>{{ $user->national_code  }}</td>
                            </tr>
                            <tr>
                                <td> شماره شناسنامه</td>
                                <td>{{ $user->birth_certificate_code  }}</td>
                            </tr>
                            <tr>
                                <td>تلفن</td>
                                <td>{{ $user->phone  }}</td>
                            </tr>
                            <tr>
                                <td>کد پستی</td>
                                <td>{{ $user->zip_code  }}</td>
                            </tr>
                            <tr>
                                <td>استان</td>
                                <td>
                                @foreach($provinces as $province)
                                    @if ($province->id == $user->province_id)
                                        {{ $province->name }}
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <td>شهر</td>
                                <td>
                                    @foreach($cities as $city)
                                        @if ($city->id == $user->city_id)
                                            {{ $city->name }}
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>آدرس</td>
                                <td>{{ $user->address  }}</td>
                            </tr>
                            <tr>
                                <td>تاریخ ثبت نام</td>
                                <td>{{ jDate($user->created_at)->format('Y/m/d H:m:s') }}</td>
                            </tr>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection