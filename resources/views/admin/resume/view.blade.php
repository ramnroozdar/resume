@extends('layouts.app')

@section('title', " رزومه شماره ", $resume->id)

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ config('platform.name') }}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">مدیریت سیستم</a></li>
                    <li class="breadcrumb-item" ><a href="{{ route('admin.resumes') }}">رزومه ها</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('admin.resumes.view',['id',$resume->id]) }}">جزئیات رزومه</a></li>
                </ol>
            </nav>
        </div>
        <div class="col-md-12">
            <h1>رزومه شماره:{{$resume->id}}</h1>
        </div>
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    جزئیات رزومه
                    <button type="button" class="btn btn-sm btn-mobile btn-primary pull-left" onclick="$('#viewUser').toggle();">
                        <i class="fa fa-plus"></i>
                        مشخصات کاربر ارسال کننده
                    </button>
                </div>
                <div class="alert alert-success mt-2" id="viewUser" role="alert" style="display: none">
                    <h4> مشخصات کاربر </h4>
                    <table class="table table-hover table-bordered table-success">
                        <tr>
                            <td>نام کاربر</td>
                            <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <td>ایمیل</td>
                            <td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <td>موبایل</td>
                            <td>{{ $user->mobile }}</td>
                        </tr>
                        <tr>
                            <td>کد ملی</td>
                            <td>{{ $user->national_code }}</td>
                        </tr>
                        <tr>
                            <td>ش شناسنامه</td>
                            <td>{{ $user->birth_certificate_code }}</td>
                        </tr>
                        <tr>
                            <td>تاریخ تولد</td>
                            <td>{{ $user->birth }}</td>
                        </tr>
                        <tr>
                            <td>جنسیت</td>
                            <td>{{ constant('App\Enums\FileEnum::GENDER_'.strtoupper($user -> gender).'_TEXT')}}</td>
                        </tr>
                        <tr>
                            <td>وضعیت نظام وظیفه</td>
                            <td>{{ constant('App\Enums\FileEnum::SOLDIER_'.strtoupper($user -> soldier).'_TEXT')}}</td>
                        </tr>
                        <tr>
                            <td>وضعیت تاهل</td>
                            <td>{{ constant('App\Enums\FileEnum::MATERIAL_'.strtoupper($user -> material).'_TEXT')}}</td>
                        </tr>
                        <tr>
                            <td>تلفن ثابت</td>
                            <td>{{ $user->phone }}</td>
                        </tr>
                        <tr>
                            <td>کد پستی</td>
                            <td>{{ $user->zip_code }}</td>
                        </tr>
                        <tr>
                            <td>استان</td>
                            <td>
                               @foreach($provinces as $province)
                                   @if ($province->id == $user->province_id)
                                       {{ $province->name }}
                                   @endif
                                   @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>شهر</td>
                            <td>
                                @foreach($cities as $city)
                                    @if ($city->id == $user->city_id)
                                        {{ $city->name }}
                                    @endif
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>آدرس کامل</td>
                            <td>{{ $user->address }}</td>
                        </tr>
                        <tr>
                            <td>تاریخ ثبت نام</td>
                            <td>{{ jDate($user->created_at)->format('Y/m/d') }}</td>
                        </tr>
                    </table>

                    <button type="button" onclick="$('#viewUser').hide();" class="btn btn-sm btn-dark">
                        <i class="fa fa-minus"></i>
                        پنهان کردن
                    </button>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">شماره رزومه:</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->id}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">شغل:</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->category->name}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">نام و نام خانوادگی:</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->user->name}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">نحوه آشنایی با ما :</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{ constant('App\Enums\FileEnum::INTRODUCTION_'.strtoupper($resume -> introduction).'_TEXT')}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">وضعیت :</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{ constant('App\Enums\FileEnum::STATUS_'.($resume -> status).'_TEXT')}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">نام فایل :</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->name}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">نوع فایل :</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->file_type}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">سایز فایل :</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{$resume->size}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 col-sm-4 col-lg-4 @lang('platform.input-pull')">تاریخ ارسال:</label>
                        <div class="col-md-8 col-lg-8 col-sm-8">
                            {{ jDate($resume->created_at)->format('Y/m/d') }}
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('admin.resumes.status',[$resume->id]) }}">
                        @CSRF
                        <div class="form-row">
                            <div class="col-md-6">
                                <select  name="status" id="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}"   >
                                    <option value="1"{{ old('status', $resume->status) == '1' ? ' selected' :'' }}>منتظر بررسی از سوی کارشناسان</option>
                                    <option value="2"{{ old('status', $resume->status) == '2' ? ' selected' :'' }}>در حال بررسی از سوی کارشناسان</option>
                                    <option value="3"{{ old('status', $resume->status) == '3' ? ' selected' :'' }}>عدم تائید-نقص اطلاعات یا عدم ارسال فایل رزومه</option>
                                    <option value="4"{{ old('status', $resume->status) == '4' ? ' selected' :'' }}>تائید اولیه - منتظر تماس یا جلسه</option>
                                    <option value="5"{{ old('status', $resume->status) == '5' ? ' selected' :'' }}>عدم تائید - ظرفیت تکمیل</option>
                                    <option value="6"{{ old('status', $resume->status) == '6' ? ' selected' :'' }}>عدم تائید نهایی</option>
                                    <option value="7"{{ old('status', $resume->status) == '7' ? ' selected' :'' }}>تائید نهایی</option>
                                </select>
                                @if ($errors->has('status'))
                                    <span class="invalid-feedback">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-block">
                                    <i class="fa fa-save"></i>
                                    ثبت وضعیت جدید برای این رزومه
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="card">
                <div class="card-header badge-danger h5">
                    پشتیبانی
                </div>
                <div class="card-body">
                    @foreach($supports as $support)
                        @if ($support->resume_id == $resume->id)
                            @if ($resume->user_id == $support->user_id )
                                <div class="card card-default bg-secondary mb-2 mt-2">
                                    <div class="card-header">
             <span class="badge badge-dark pull-left">
                 {{ $support->created_at }}
             </span>
                                        <div class="row">
                <span class="mt-2">
                    {{ $support->user->name }}
                </span>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        {!! nl2br($support->comment)  !!}
                                        <form id="comment_{{$support->id}}" style="display: none">
                                            <textarea class="form-control"></textarea>
                                            <button>ثبت</button>
                                        </form>
                                    </div>
                                </div>
                            @else
                                <div class="card card-default ml-5 mb-2 bg-success">
                                    <div class="card-header">
                                        <span class="badge badge-dark pull-left">{{ $support->created_at }}</span>
                                        <div class="row">
                <span class="mt-2 text-white">
                      <i class="fa fa-headphones fa-2x text-secondary"></i>{{ $support->user->name }}  (پشتیبانی)
                </span>
                                        </div>
                                    </div>
                                    <div class="card-body text-white">
                                        {!! nl2br($support->comment)  !!}
                                    </div>
                                </div>
                            @endif
                        @endif
                    @endforeach
                        {{ $supports->links() }}
                    <div class="card card-default">
                        <div class="card-body">
                            <form method="POST" action="{{ route('frontend.resumes.comment',['id'=>$resume->id]) }}" enctype="multipart/form-data">
                                @csrf
                                @method('post')
                                <div class="form-group">
                                    <label for="text">متن</label>
                                    <textarea name="comment" id="comment" class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}">{{old('comment')}}</textarea>
                                    @if ($errors->has('comment'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('comment') }}</strong></span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary btn-mobile"><i class="fa fa-send"></i>ارسال پاسخ به کاربر</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="card">
                <div class="card-header badge-danger h5">
                    کامنت مخصوص مدیران غیر قابل مشاهده برای کاربران عادی
                </div>
                <div class="card-body">
                    @foreach($comments as $comment)
                        @if ($comment->resume_id == $resume->id)
                            <div class="card card-default bg-secondary mb-2 mt-2">
                                    <div class="card-header">
                                         <span class="badge badge-dark pull-left">{{ $comment->created_at }}</span>
                             <div class="row">
                                     <span class="mt-2"><i class="fa fa-headphones fa-2x"></i>{{ $support->user->name }}</span>
                             </div>
                                    </div>
                                    <div class="card-body">
                                        {!! nl2br($comment->comment)  !!}
                                        <form id="comment_{{$comment->id}}" style="display: none">
                                            <textarea class="form-control"></textarea>
                                            <button>ثبت</button>
                                        </form>
                                    </div>
                                </div>
                        @endif
                    @endforeach
                        {{ $comments->links() }}
                    <div class="card card-default">
                        <div class="card-body">
                            <form method="POST" action="{{ route('admin.resumes.comment',['id'=>$resume->id]) }}" enctype="multipart/form-data">
                                @csrf
                                @method('post')
                                <div class="form-group">
                                    <label for="text">متن</label>
                                    <textarea name="comment" id="comment" class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}">{{old('comment')}}</textarea>
                                    @if ($errors->has('comment'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('comment') }}</strong></span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary btn-mobile"><i class="fa fa-send"></i>ارسال کامنت</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
