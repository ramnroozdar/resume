@extends('layouts.app')

@section('title', "لیست رزومه ها")

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ config('platform.name') }}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">مدیریت سیستم</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('admin.resumes') }}">رزومه ها</a></li>
                </ol>
            </nav>
        </div>
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    مشاهده رزومه های مربوط به یک شغل
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.transactions.resume-cat') }}">
                        @CSRF
                        <div class="form-row">
                            <div class="col-md-6">
                                <select class="form-control" name="category" id="category">
                                    @foreach($categories as $category)
                                        <option value="{{  $category->id }}" {{old('category') == $category->id ? ' selected' : ''}}>{{ $category->name  }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-danger btn-block">
                                    <i class="fa fa-search"></i>
                                    جستجو
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
                <div class="card card-default">
                    <div class="card-header">  لیست فایل های دسته بندی : {{ $categoryItem->name }}  </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered table-hover">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col" class="text-center">#</th>
                                <th scope="col" class="text-center">نام و نام خانوادگی</th>
                                <th scope="col" class="text-center">تاریخ</th>
                                <th scope="col" class="text-center">اقدام ها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($categoryResumes && count($categoryResumes)>0)
                            @foreach($categoryResumes as $resume)
                                <tr>
                                    <td scope="row" class="text-center">{{ $resume->id }}</td>

                                    <td class="text-center table-danger">{{ $resume->user->name}}</td>

                                    <td class="text-center">{{ $resume->created_at }}</td>
                                    <td>
                                        {{--<button type="button" onclick="$('#viewResume').toggle();" class="btn btn-mobile btn-success btn-sm"><i class="fa fa-eye"></i> نمایش</button>--}}
                                        <a href="{{ route('admin.resumes.view',['id'=> $resume->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i> نمایش</a>
                                        <a href="{{ route('admin.resumes.download',['id'=> $resume->id]) }}" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> دانلود</a>
                                    </td>
                                </tr>
                            @endforeach
                                @else
                                <tr>
                                    <td colspan="4">
                                        <span>هیچ رزومه ای برای این شغل وجود ندارد </span>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>



        </div>
    </div>
    </div>
    </div>

@stop