@extends('layouts.app')

@section('content')
    <main class="py-4" role="main">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ config('platform.name') }}</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="{{route('frontend.users.dashboard')}}">داشبرد</a></li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-12">
                    <h1>داشبرد</h1>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="list-group mb-2d-none d-md-block d-lg-block d-xl-block">
                        <a class="list-group-item list-group-item-action active" href="{{ route('admin.users') }}"><i class="fa fa-dashboard"></i> کابران</a>
                        <a class="list-group-item list-group-item-action" href="{{ route('admin.resumes') }}"><i class="fa fa-files-o"></i> رزومه ها</a>
                        <a class="list-group-item list-group-item-action" href="{{ route('admin.categories') }}"><i class="fa fa-object-group"></i> شغل ها</a>
                    </div>
                </div>
                <div class="col-md-9">

                    <div class="row justify-content-center">
                        <div class="col-md-12 col-xs-12">
                            <div class="alert alert-info">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                امروز {{ jDate(now())->format('Y/m/d') }}
                            </div>
                        </div>

                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <a class="mb-1 btn btn-info btn-block" href="#" ><i class="fa fa-files-o"></i> رزومه ها </a>
                        </div>
                        <div class="col-md-4">
                            <a class="mb-1 btn btn-success btn-block" href="#" ><i class="fa fa-users"></i> کاربران </a>
                        </div>
                        <div class="col-md-4">
                            <a class="mb-1 btn btn-danger btn-block" href="#"><i class="fa fa-object-group"></i> شغل ها</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </main>

@endsection
