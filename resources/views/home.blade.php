@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="jumbotron">
                <h4 class="display-6">
                    @if (Auth::guest())
                        سلام مهمان گرامی
                    @else
                        سلام کاربر گرامی  {{ Auth::user()->name }}
                    @endif
                </h4>
                <p class="lead">شرکت فناوری صبانوین جام جم (سهامی خاص) در جهت گسترش خدمات خود همواره نیروهای متخصص و خلاق را جذب می نماید. توجه داشته باشید که هرچه نمونه کار و رزومه شما دقیق تر باشد در جذب شما تاثیر بسزائی خواهد داشت.</p>
                <hr class="my-4">
                <p>شرکت صبانوین جام جم در حال حاضر در زمینه های کاری زیر نیرو می پذیرد.</p>
                <ul>
                    @foreach($categories as $category)
                    <li> {{ $category->name }} </li>
                    @endforeach
                </ul>
                <p>نکته : توجه داشته باشید که ارسال رزومه هیچ گونه تعهدی را برای جذب شما ایجاد نمی کند.</p>
            </br>

                <div class="col-md-12 row justify-content-center">
                    @guest
                        <a class="btn btn-success btn-block mr-1 col-md-10" href="{{ route('frontend.resume.create') }}" role="button"><i class="fa fa-file"></i> ارسال رزومه برای صبانوین </a>
                    @endguest
                    @auth
                            <a class="btn btn-warning btn-lg btn-sm col-md-4 mr-1 " href="{{ route('users.profile') }}" role="button"><i class="fa fa-user-circle-o"></i> تکمیل مشخصات کاربری </a>
                            <a class="btn btn-dark btn-lg btn-sm col-md-4" href="{{ route('users.resume') }}" role="button"><i class="fa fa-file"></i> ارسال رزومه برای صبانوین </a>
                    @endauth
                </div>

            </div>

        </div>
    </div>
</div>
@endsection
