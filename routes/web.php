<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/access', 'HomeController@access')->name('access');

Route::get('/welcome', 'HomeController@welcome')->name('home.welcome');

    //dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->middleware('auth');

    //user
    Route::get('/user/dashboard','UsersController@dashboard')->name('frontend.users.dashboard')->middleware('auth');
    Route::get('/user/password', 'UsersController@password')->name('frontend.users.password')->middleware('auth');
    Route::post('/user/password', 'UsersController@update_password')->name('frontend.users.update_password')->middleware('auth');
    Route::get('/user/profile','UsersController@profile')->name('users.profile')->middleware('auth');
    Route::post('/user/profile', 'UsersController@updateProfile')->name('frontend.users.updateProfile')->middleware('auth');
    Route::post('/user/information', 'UsersController@updateInformation')->name('frontend.users.updateInformation')->middleware('auth');
    Route::post('/user/profile/cities', 'UsersController@cities')->name('frontend.users.profile.cities')->middleware('auth');


    //resume
    Route::get('/user/resume','UsersController@resume')->name('users.resume')->middleware('auth');
    Route::post('/resume/insert','ResumeController@insert')->name('resume.insert')->middleware('auth');
    Route::get('/resumes','ResumeController@resume')->name('frontend.resumes')->middleware('auth');
    Route::get('/resumes/view/{id}','ResumeController@view')->name('frontend.resumes.view')->middleware('auth');
    Route::get('/resume/create','ResumeController@create')->name('frontend.resume.create');
    Route::post('/resume/store','ResumeController@store')->name('frontend.resume.store');



    Route::post('/resumes/comment/{id}','SupportController@comment')->name('frontend.resumes.comment')->middleware('auth');


//Notification
Route::get('/notification', 'NotificationController@index')->name('frontend.notification')->middleware('auth');
Route::get('/notification/view/{id}', 'NotificationController@view')->name('frontend.notification.view')->middleware('auth');
Route::get('/notification/count-unread', 'NotificationController@countUnread')->name('frontend.notification.count-unread');
Route::get('/notification/get-unread', 'NotificationController@getUnread')->name('frontend.notification.get-unread');



Route::group(['prefix' => 'admin', 'namespace' => 'Admin','middleware' => 'admin'], function () {

    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');


    // users routes
    Route::get('/users', 'UsersController@index')->name('admin.users');
    Route::get('/users/view/{user_id}', 'UsersController@view')->name('admin.users.view');




    Route::get('/users/data', 'UsersController@data')->name('admin.user.data');
    Route::get('/users/create', 'UsersController@create')->name('admin.users.create');
    Route::post('/users/create', 'UsersController@insert')->name('admin.users.insert');
    Route::get('/users/edit/{user_id}', 'UsersController@edit')->name('admin.users.edit');

    Route::post('/users/edit/{user_id}', 'UsersController@update')->name('admin.users.update');
    Route::delete('/users/delete/{user_id}', 'UsersController@delete')->name('admin.users.delete');
    Route::post('/users/restore/{user_id}', 'UsersController@restore')->name('admin.users.restore');
    Route::delete('/users/force-delete/{user_id}', 'UsersController@forceDelete')->name('admin.users.force-delete');
    Route::get('/users/files/{user_id}', 'UsersController@files')->name('admin.users.files');

    //files routes
    Route::get('/resumes', 'ResumeController@index')->name('admin.resumes');
    Route::get('/resume/view/{id}', 'ResumeController@view')->name('admin.resumes.view');
    Route::post('/resume/status/{id}', 'ResumeController@status')->name('admin.resumes.status');
    Route::get('/resume/download/{id}','ResumeController@download')->name('admin.resumes.download');
    Route::post('/resume/resume-cat', 'ResumeController@resumeCat')->name('admin.transactions.resume-cat');

    Route::post('/resumes/comment/{id}','CommentController@comment')->name('admin.resumes.comment');


    //jobs
    Route::get('/categories', 'CategoriesController@index')->name('admin.categories');
    Route::get('/categories/create', 'CategoriesController@create')->name('admin.categories.create');
    Route::post('/categories/create', 'CategoriesController@insert')->name('admin.categories.insert');
    Route::get('/categories/edit/{id}', 'CategoriesController@edit')->name('admin.categories.edit');
    Route::post('/categories/edit/{id}', 'CategoriesController@update')->name('admin.categories.update');
    Route::post('/categories/disable/{id}', 'CategoriesController@disable')->name('admin.categories.disable');
    Route::post('/categories/accepted/{id}', 'CategoriesController@accepted')->name('admin.categories.accepted');

    Route::get('/files/create', 'FilesController@create')->name('admin.files.create');
    Route::post('/files/create', 'FilesController@insert')->name('admin.files.insert');
    Route::get('/files/edit/{file_id}', 'FilesController@edit')->name('admin.files.edit');
    Route::get('/files/view/{file_id}', 'FilesController@view')->name('admin.files.view');
    Route::post('/files/edit/{file_id}', 'FilesController@update')->name('admin.files.update');
    Route::delete('/files/delete/{file_id}', 'FilesController@delete')->name('admin.files.delete');
    Route::post('/files/restore/{file_id}', 'FilesController@restore')->name('admin.files.restore');
    Route::delete('/files/force-delete/{file_id}', 'FilesController@forceDelete')->name('admin.files.force-delete');
    Route::post('/files/accepted/{file_id}', 'FilesController@accepted')->name('admin.files.accepted');
    Route::post('/files/unaccepted/{file_id}', 'FilesController@unaccepted')->name('admin.files.unaccepted');
    Route::post('/files/disable/{file_id}', 'FilesController@disable')->name('admin.files.disable');

    Route::get('/files/files_buy','FilesController@files_buy')->name('admin.files.files_buy');
    Route::get('/files/view_buy/{id}','FilesController@view_buy')->name('admin.files.view_buy');







});