<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Resume;
use App\Models\Support;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Notifications\TicketCreated;
use App\Notifications\TicketReplaid;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    public function comment($id, Request $request)
    {
        $request->validate([
            'comment' => 'required|string|min:5|max:5000',
        ]);
        $resume = Resume::findOrFail($id);
        $support = new Support();
        $support->user_id = Auth::user()->id;
        $support->comment = $request->comment;
        $support->resume_id = $resume->id;
        $support->save();

        if (Auth::user()->role != 'admin')
        {
            $user = User::findOrFail(config('platform.main-admin-user-id'));
            $user->notify(new TicketCreated($resume , $user));
        }

        return redirect()->back()->with('success','پیام شما با موفقیت برای پشتیبانی ارسال گردید');
    }
}
