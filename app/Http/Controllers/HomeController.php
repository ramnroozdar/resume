<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::accepted()->orderBy('order', 'asc')->get();
        return view('home',compact('categories'));
    }

    public function access()
    {
     return view('frontend.user.access');
    }
}
