<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Resume;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\City;
use App\Models\Province;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class UsersController extends Controller
{
    public function profile()
    {
        $provinces = Province::all();
        if(Auth::user()->province_id) {
            $cities = City::where('province_id', Auth::user()->province_id)->get();
        } else {
            $cities = City::where('province_id', $provinces->first()->id)->get();
        }
        return view('frontend.user.profile',compact('cities','provinces'));
    }
    public function cities(Request $request)
    {
        $cities = City::where('province_id', $request->province_id)->get();
        return response()->json($cities);
    }

    public function updateProfile(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        //$user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->save();
        return redirect()->route('users.profile')->with('success','اطلاعات با موفقیت به روز گردید.');
    }
    public function updateInformation(Request $request)
    {
        Validator::make($request->all(), [
            'national_code' => 'required||numeric|unique:users,national_code,' . Auth::user()->id,
            'birth_certificate_code' => 'required|numeric',
            'birth' => 'required|string',
            'material' => 'required|string',
            'phone' => 'required|numeric',
            'zip_code' => 'required|numeric',
            'address' => 'required|string',
            'gender' => 'required|string',
            'soldier' => 'required|string',
        ])->validate();

        $user = User::findOrFail(Auth::user()->id);
        $user->material= $request->material;
        $user->birth= $request->birth;
        $user->national_code = $request->national_code;
        $user->birth_certificate_code = $request->birth_certificate_code;
        $user->zip_code = $request->zip_code;
        $user->gender = $request->gender;
        $user->soldier = $request->soldier;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->status = $request->status;
        $user->city_id = $request->city_id;
        $user->province_id = $request->province_id;
        $user->save();
        return redirect()->route('frontend.resumes')->with('success','اطلاعات با موفقیت به روز گردید.');
    }

    public function resume()
    {
        $categories = Category::accepted()->orderBy('order', 'asc')->get();
        return view('frontend.user.resume',compact('categories','resumes'));
    }
    public function password()
    {
        return view('frontend.user.password');
    }
    public function update_password(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|confirmed|min:6|different:current_password',
            'password_confirmation' => 'required|min:6|different:current_password',
        ]);

        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with('error','رمزعبور فعلی شما با گذرواژه ارائه شده شما مطابقت ندارد. لطفا دوباره تلاش کنید.');
        }

        if(strcmp($request->get('current_password'), $request->get('password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with('error','رمز عبور جدید نمی تواند همان رمز عبور فعلی شما باشد. لطفا یک رمز عبور دیگر انتخاب کنید ');
        }

        $user = Auth::user();
        $user->password = bcrypt($request->get('password'));
        $user->save();

        return redirect()->route('frontend.users.password')->with('success','رمز با موفقیت ویرایش شد.');
    }
}
