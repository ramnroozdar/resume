<?php

namespace App\Http\Controllers\Admin;

use App\Models\Resume;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function comment($id, Request $request)
    {
        $request->validate([
            'comment' => 'required|string|min:5|max:5000',
        ]);
        $resume = Resume::findOrFail($id);
        $comment = new Comment();
        $comment->user_id = Auth::user()->id;
        $comment->comment = $request->comment;
        $comment->resume_id = $resume->id;
        $comment->save();

        return redirect()->back()->with('success','پیام شما با موفقیت برای پشتیبانی ارسال گردید');
    }
}
