<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Resume;
use App\Models\Support;
use App\Models\Comment;
use App\Models\Province;
use App\Models\City;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResumeController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $resumes = Resume::orderBy('created_at','desc')->paginate(config('platform.per-page'));
        return view('admin.resume.index',compact('resumes','categories'));
    }

    public function view($id , Request $request)
    {
        $provinces = Province::all();
        $resume = Resume::findOrFail($id);
        $user = User::findOrFail($resume->user_id);
        if($user->province_id) {
            $cities = City::where('province_id', $user->province_id)->get();
        } else {
            $cities = City::where('province_id', $provinces->first()->id)->get();
        }
        $supports = Support::orderBy('created_at','desc')->paginate(config('platform.per-page'));
        $comments = Comment::orderBy('created_at','desc')->paginate(config('platform.per-page'));
        return view('admin.resume.view',compact('resume','supports','comments', 'user','provinces','cities'));

    }
    public function download($id , Request $request)
    {
        $resume = Resume::findOrFail($id);
        return Storage::download($resume->resume,$resume->id.".".$resume->file_type);
    }
    public function resumeCat(Request $request)

    {
        $categories = Category::all();

        $validator = Validator::make($request->all(),[
            'category' => 'required|string',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('admin.resumes')->withInput($request->all())->withErrors($validator);
        }
        $category = $request->category;
        $categoryItem = Category::find($request->category);
        $categoryResumes = $categoryItem->resumes;

        return view('admin.resume.resumeCat',compact('category','categoryResumes','categories','categoryItem'))->withInput($request->all());
    }

    public function status($id , Request $request)
    {
        $resume = Resume::findOrFail($id);
        $resume->status = $request->status;
        $resume->save();
        return redirect()->back()->with('success','وضعیت جدید با موفقیت ثبت گردید');
    }
}
