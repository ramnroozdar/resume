<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.category.index',compact('categories'));
    }
    public function create()
    {
        return view('admin.category.create');
    }
    public function insert(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'order' => 'numeric|nullable',
        ]);
        $category = new Category();
        $category->name = $request->name;
        $category->order = $request->order;
        $category->save();
        return redirect()->route('admin.categories')->with('success','شغل با موفقیت ایجاد گردید.');
    }
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.category.edit', compact('category'));
    }
    public function update(Request $request,$id )
    {
        $request->validate([
            'name' => 'required|string',
            'order' => 'numeric|nullable',
        ]);
        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->order = $request->order;
        $category->save();
        return redirect()->route('admin.categories')->with('success','اطلاعات با موفقیت به روزرسانی گردید.');
    }
    public function disable($id, Request $request)
    {
        $category = Category::findOrFail($id);
        $category -> status = 'disable';
        $category->save();
        return redirect()->back()->with('success','شغل مورد نظر غیرفعال گردید.');
    }
    public function accepted($id, Request $request)
    {
        $category = Category::findOrFail($id);
        $category -> status = 'accepted';
        $category->save();
        return redirect()->back()->with('success','شغل مورد نظر فعال گردید.');
    }
}
