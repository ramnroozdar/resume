<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\City;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::paginate(config('platform.per-page'));
        return view('admin.user.index',compact('users'));
    }
    public function view($id)
    {
        $provinces = Province::all();
        if(Auth::user()->province_id) {
            $cities = City::where('province_id', Auth::user()->province_id)->get();
        } else {
            $cities = City::where('province_id', $provinces->first()->id)->get();
        }
        $user = User::findOrFail($id);
        return view('admin.user.view',compact('user','provinces','cities'));
    }
}
