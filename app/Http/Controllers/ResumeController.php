<?php

namespace App\Http\Controllers;

use App\Models\Support;
use App\Models\Category;
use App\Models\SupportPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Resume;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Notification;
use App\Notifications\TicketCreated;
use App\Notifications\CreatedResume;

class ResumeController extends Controller
{
    public function resume ()
    {
        $resumes = Resume::where('user_id' , Auth::user()->id)->paginate(config('platform.per-page'));
        return view('frontend.resume.index',compact('resumes'));
    }

    public function view($id)
    {
        $resume = Resume::findOrFail($id);
        if (Auth::user()->role == 'admin' || Auth::user()->id == $resume->user_id)
        {
            $supports = Support::orderBy('created_at', 'asc')->paginate(config('platform.per-page'));
            return view('frontend.resume.view', compact('resume', 'supports'));
        }
        return redirect()->route('access');

    }
    public function insert(Request $request)
    {

            $request->validate([
                'resume' => 'required|max:2000|mimes:doc,docx,pdf,zip',
            ]);

        $resume = new Resume();
        $resume->user_id = Auth::user()->id;
        $resume->category_id = request('job');
        $resume->introduction = request('introduction');
        if($request->hasFile('resume')) {
            $resume_path = $request->file('resume')->store('files');
            $resume->name = $request->file('resume')->getClientOriginalName();
            $resume->resume = $resume_path;
            $resume->file_type = $request->file('resume')->getClientOriginalExtension();
            $resume->size = $request->file('resume')->getSize();
        }
        $resume->save();
        $user = User::findOrFail(1);
        try {
            Notification::send($user, new CreatedResume($resume , $user));
        } catch (\Exception $e) {}


        //        $user->notify(new CreatedResume($resume , $user));
        return redirect()->route('dashboard')->with('success','رزومه با موفقیت ارسال گردید.');

    }

    public function create()
    {
        $categories = Category::all();
        return view('frontend.resume.create',compact('categories'));
    }

    public function store(Request $request){

            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email',
                'mobile' => 'required|string|max:11|unique:users,mobile',
                'resume' => 'max:2000|required|mimes:doc,docx,pdf,zip',
            ]);

        $user = new User();
        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->password = Hash::make($request->mobile);
        $user->save();
        $resume = new Resume();
        $resume->user_id = $user->id;
        $resume->category_id = request('job');
        $resume->introduction = request('introduction');
        if($request->hasFile('resume')) {
            $resume_path = $request->file('resume')->store('files');
            $resume->name = $request->file('resume')->getClientOriginalName();
            $resume->resume = $resume_path;
            $resume->file_type = $request->file('resume')->getClientOriginalExtension();
            $resume->size = $request->file('resume')->getSize();
        }
        $resume->save();
        $admin = User::findOrFail(1);
        try {
            Notification::send($admin, new CreatedResume($resume , $admin));
        } catch (\Exception $e) {}
        return view('frontend.resume.details',compact('user'));
    }

}
