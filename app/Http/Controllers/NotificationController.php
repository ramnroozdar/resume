<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);
        $notifications = $user->unreadNotifications;
        return view('frontend.notification.index',compact('notifications'));
    }

    public function view($id)
    {
        $notification = Auth::user()->notifications()->findOrFail($id);
        $notification->markAsRead();
        return redirect($notification->data['url']);
    }
    public function countUnread()
    {
        return response()->json([
            'unread' => Auth::user()->unreadNotifications->count()
        ]);
    }
    public function getUnread()
    {
        return response()->json([
            'unread' => Auth::user()->unreadNotifications
        ]);
    }}
