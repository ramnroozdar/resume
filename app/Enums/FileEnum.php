<?php
namespace App\Enums;

class FileEnum {
    const STATUS_1 = '1';
    const STATUS_1_TEXT = 'منتظر بررسی از سوی کارشناسان';

    const STATUS_2 = '2';
    const STATUS_2_TEXT = 'در حال بررسی از سوی کارشناسان';

    const STATUS_3 = '3';
    const STATUS_3_TEXT = 'عدم تائید-نقص اطلاعات یا عدم ارسال فایل رزومه';

    const STATUS_4= '4';
    const STATUS_4_TEXT = 'تائید اولیه - منتظر تماس یا جلسه';

    const STATUS_5= '5';
    const STATUS_5_TEXT = 'عدم تائید - ظرفیت تکمیل';

    const STATUS_6= '6';
    const STATUS_6_TEXT = 'عدم تائید نهایی';

    const STATUS_7= '7';
    const STATUS_7_TEXT = 'تائید نهایی';



    const ROLE_ADMIN = 'admin';
    const ROLE_ADMIN_TEXT = 'مدیر';

    const ROLE_USER = 'user';
    const ROLE_USER_TEXT = 'کاربر عادی';

    const ROLE_STAFF = 'staff';
    const ROLE_STAFF_TEXT = 'کارمند';

    const ROLE_MARKETER = 'marketer';
    const ROLE_MARKETER_TEXT = 'بازاریاب';

    const GENDER_MALE = 'male';
    const GENDER_MALE_TEXT = 'مرد';

    const GENDER_FEMALE = 'female';
    const GENDER_FEMALE_TEXT = 'زن';


    const MATERIAL_SINGLE = 'single';
    const MATERIAL_SINGLE_TEXT = 'مجرد';

    const MATERIAL_MARRIED = 'married';
    const MATERIAL_MARRIED_TEXT = 'متاهل';

    const LAST_GRADE_1 = '1';
    const LAST_GRADE_1_TEXT = 'دیپلم';
    const LAST_GRADE_2 = '2';
    const LAST_GRADE_2_TEXT = 'فوق دیپلم';
    const LAST_GRADE_3 = '3';
    const LAST_GRADE_3_TEXT = 'لیسانس';
    const LAST_GRADE_4 = '4';
    const LAST_GRADE_4_TEXT = 'فوق لیسانس';
    const LAST_GRADE_5 = '5';
    const LAST_GRADE_5_TEXT = 'دکترا';
    const LAST_GRADE_6 = '6';
    const LAST_GRADE_6_TEXT = 'هیچکدام';

    const INTRODUCTION_1 = '1';
    const INTRODUCTION_1_TEXT = 'موتورهای جستجو';
    const INTRODUCTION_2 = '2';
    const INTRODUCTION_2_TEXT = 'پیامک تبلیغاتی';
    const INTRODUCTION_3 = '3';
    const INTRODUCTION_3_TEXT = 'معرفی دوستان';
    const INTRODUCTION_4 = '4';
    const INTRODUCTION_4_TEXT = 'تبلیغات در سایت ها';
    const INTRODUCTION_5 = '5';
    const INTRODUCTION_5_TEXT = 'ایمیل تبلیغاتی';
    const INTRODUCTION_6 = '6';
    const INTRODUCTION_6_TEXT = 'سایر';


    const SOLDIER_1 = '1';
    const SOLDIER_1_TEXT = 'پایان خدمت';
    const SOLDIER_2 = '2';
    const SOLDIER_2_TEXT = 'معافیت پزشکی';
    const SOLDIER_3 = '3';
    const SOLDIER_3_TEXT = 'سایر معافیت ها';
    const SOLDIER_4 = '4';
    const SOLDIER_4_TEXT = 'انجام نشده';
    const SOLDIER_5 = '5';
    const SOLDIER_5_TEXT = 'زن می باشم';

}


