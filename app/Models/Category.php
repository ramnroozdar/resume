<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function resumes()
    {
        return $this->hasMany('App\Models\Resume','category_id');
    }
    public function scopeAccepted($query)
    {
        return $query->where('status','accepted');
    }
}
