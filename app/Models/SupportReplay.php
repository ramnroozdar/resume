<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportReplay extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function support()
    {
        return $this->belongsTo('App\Models\Support');
    }
}
