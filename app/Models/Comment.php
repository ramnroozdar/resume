<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function resume()
    {
        return $this->belongsTo('App\Models\Resume');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function replays()
    {
        return $this->hasMany('App\Models\SupportReplay');
    }}
