<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Channels\TextMessageChannel;
use App\Channels\TextMessage;

class TicketCreated extends Notification
{
    use Queueable;
    public $resume = null;
    public $user = null;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($resume, $user)
    {
        $this->resume = $resume;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', TextMessageChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => "تیکت جدید ارسال شد" ,
            'url' => route('frontend.resumes.view', [$this->resume->id]),
        ];
    }
    public function toTextMessage($notifiable)
    {
        $url = route('frontend.resumes.view',[$this->resume->id]);
        return TextMessage::create()
            ->to($this->user->mobile)
            ->content('تیکت جدید ارسال شد:'."\r\n" . $url);
    }
}
